# # encoding: UTF-8

# require 'uz_rand'
# include UzRand

# SettingGroup.reset

# lorem_one_paragraph = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."

# lorem = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. orem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."

# t = "-need to be set in admin page-"

# group = "Main"
# g = SettingGroup.seeds_create(main_group: group, display_name: "General", key: :general)
# g.create_string(display_name: "Website Title", key: :website_title, value: "Sinners Club")

# meta_tag_description = "Um brinde às noites em claro, aos amigos e tragos. Às más companias, às boas companias, aos amores de uma vida e aqueles que duram apenas uma noite. 
# Aos desajustados e aos que não se encaixam.
# Bem vindo a Sinners, a nova casa noturna de Porto Alegre, onde o ontem é passado e o amanhã não existe."

# meta_tag_keywords = "sinners casa noturna porto alegre club sinnersclub alternativa lab labpoa"

# g = SettingGroup.seeds_create(main_group: group, display_name: "SEO Meta Tags", key: :seo)
# g.create_text(display_name: "Meta Tag Description", key: :meta_tag_description, value: meta_tag_description)
# g.create_text(display_name: "Meta Tag Keywords", key: :meta_tag_keywords, value: meta_tag_keywords)

# g = SettingGroup.seeds_create(main_group: group, display_name: "Google Analytics", key: :google_analytics)
# g.create_string(display_name: "Tracker", key: :tracker, value: "")
# g.create_string(display_name: "Domain", key: :domain, value: "sinnersclub.com.br")

# g = SettingGroup.seeds_create(main_group: group, display_name: "Facebook / Sharing", key: :social)
# g.create_text(display_name: "OG Default Description", key: :og_description, value: meta_tag_description)
# g.create_image(display_name: "OG Default Image", key: :og_image, value: "facebook.jpg")
# g.create_string(display_name: "App Id", key: :facebook_app_id, value: "154874548036677")
# g.create_string(display_name: "Admins User Ids", key: :facebook_admins_user_ids, value: "leouznw", hint: "split with comas")
# g.create_string(display_name: "Base Website Url", key: :base_url, value: "http://sinnersclub.com.br")

# group = "Content"
# g = SettingGroup.seeds_create(main_group: group, display_name: "Header Social Links", key: :social_links)
# g.create_string(display_name: "Facebook Link", key: :facebook, value: "https://www.facebook.com/WelcomeToSinners")
# g.create_string(display_name: "Twitter Link", key: :twitter, value: "http://twitter.com/sinnersclub")

# g = SettingGroup.seeds_create(main_group: group, display_name: "Index Page", key: :index_page)
# g.create_image(display_name: "Background", key: :background, value: nil)
# g.create_string(display_name: "Number of next events", key: :next_events, value: "3")

# aniversarios_text = "Quer comemorar teu aniver com a gente? De vantagens pra ti, temos:
# Aniversariante da semana (4 dias antes ou depois da festa): Entrada FREE // Lista com ATÉ 15 amigos que tem direito a entrada pela fila preferencial e preço de lista sem limite de horário
# A lista deve ser enviada até as 20h do dia da festa escolhida."

# g = SettingGroup.seeds_create(main_group: group, display_name: "Aniversarios Page", key: :aniversarios_page)
# g.create_text(display_name: "Text", key: :text, value: aniversarios_text)
# g.create_string(display_name: "Telefone", key: :telefone, value:  "(051) 9333-5878")
# g.create_string(display_name: "Email", key: :email, value:  "<a href='mailto:contato@sinnersclub.com.br'>contato<em>@sinnersclub.com.br</em></a>")

# g = SettingGroup.seeds_create(main_group: group, display_name: "Contato Page", key: :contato_page)
# g.create_string(display_name: "Top", key: :top, value:  "<a href='mailto:contato@sinnersclub.com.br'>contato<em>@sinnersclub.com.br</em></a> // <span>(051) 9333-5878</span>")
# g.create_string(display_name: "Bottom", key: :bottom, value:  "R. Gen. Lima e Silva, 426 - Cidade Baixa - POA - RS.")

# sobre_text = "✞✞ The only faith we have is faith in us. ✞✞

# Um brinde às noites em claro, aos amigos e tragos. Às más companias, às boas companias, aos amores de uma vida e aqueles que duram apenas uma noite.
# Aos desajustados e aos que não se encaixam.
# Bem vindo a Sinners, um lugar onde o ontem é passado e o amanhã não existe.

# If at some point we all succumb
# For goodness sake, let us be young
# 'Cause time gets harder to outrun
# And I'm nobody, I'm not done"

# g = SettingGroup.seeds_create(main_group: group, display_name: "Sobre Page", key: :sobre_page)
# g.create_text(display_name: "Text", key: :text, value: sobre_text)

# g.create_image(display_name: "Photo 1", key: :photo1, value: nil)
# g.create_image(display_name: "Photo 2", key: :photo2, value: nil)
# g.create_image(display_name: "Photo 3", key: :photo3, value: nil)
# g.create_image(display_name: "Photo 4", key: :photo4, value: nil)
# g.create_image(display_name: "Photo 5", key: :photo5, value: nil)
# g.create_image(display_name: "Photo 6", key: :photo6, value: nil)
# g.create_image(display_name: "Photo 7", key: :photo7, value: nil)
# g.create_image(display_name: "Photo 8", key: :photo8, value: nil)


# g = SettingGroup.find_by_key(:social_links)
# g.create_string(display_name: "Instagram Link", key: :instagram, value: "https://www.instagram.com/WelcomeToSinners")

