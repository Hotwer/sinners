class UpdateToBsAdmin < ActiveRecord::Migration
  def change
    rename_table :assets, :bs_admin_assets    
    rename_table :setting_groups, :bs_admin_setting_groups
    rename_table :string_settings, :bs_admin_string_settings
    rename_table :text_settings, :bs_admin_text_settings
    rename_table :boolean_settings, :bs_admin_boolean_settings

    add_column :bs_admin_setting_groups, :hint, :text    
  end
end
