class AddBirthdayListsToEvent < ActiveRecord::Migration
  def change
    add_column :events, :has_birthday_lists, :boolean, :default => true
  end
end
