class CreateGalleries < ActiveRecord::Migration
  def change
    create_table :galleries do |t|
      t.string :party
      t.string :edition
      t.string :permalink
      t.string :cover_image
      t.datetime :date
      t.boolean :show

      t.timestamps
    end
  end
end
