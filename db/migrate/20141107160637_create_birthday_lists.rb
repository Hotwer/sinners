class CreateBirthdayLists < ActiveRecord::Migration
  def change
    create_table :birthday_lists do |t|
      t.string :name
      t.date :date
      t.string :email
      t.integer :event_id

      t.string :guest1
      t.string :guest2
      t.string :guest3
      t.string :guest4
      t.string :guest5
      t.string :guest6
      t.string :guest7
      t.string :guest8
      t.string :guest9
      t.string :guest10
      t.string :guest11
      t.string :guest12
      t.string :guest13
      t.string :guest14
      t.string :guest15

      t.timestamps
    end
  end
end
