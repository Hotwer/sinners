class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :permalink
      t.string :party
      t.string :edition
      t.datetime :date
      t.string :cover_image
      t.text :description
      t.boolean :has_guests
      t.boolean :list_closed
      t.boolean :show

      t.timestamps
    end
  end
end
