# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20160209105246) do

  create_table "birthday_lists", :force => true do |t|
    t.string   "name"
    t.date     "date"
    t.string   "email"
    t.integer  "event_id"
    t.string   "guest1"
    t.string   "guest2"
    t.string   "guest3"
    t.string   "guest4"
    t.string   "guest5"
    t.string   "guest6"
    t.string   "guest7"
    t.string   "guest8"
    t.string   "guest9"
    t.string   "guest10"
    t.string   "guest11"
    t.string   "guest12"
    t.string   "guest13"
    t.string   "guest14"
    t.string   "guest15"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "bs_admin_assets", :force => true do |t|
    t.string   "type"
    t.string   "group"
    t.string   "file"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "bs_admin_boolean_settings", :force => true do |t|
    t.string   "type"
    t.integer  "setting_group_id"
    t.string   "key"
    t.string   "display_name"
    t.string   "hint"
    t.boolean  "has_user_changed", :default => false
    t.boolean  "value"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
  end

  create_table "bs_admin_setting_groups", :force => true do |t|
    t.string   "key"
    t.string   "display_name"
    t.string   "main_group"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.text     "hint"
  end

  create_table "bs_admin_string_settings", :force => true do |t|
    t.string   "type"
    t.integer  "setting_group_id"
    t.string   "key"
    t.string   "display_name"
    t.string   "value"
    t.string   "hint"
    t.boolean  "has_user_changed", :default => false
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
  end

  create_table "bs_admin_text_settings", :force => true do |t|
    t.string   "type"
    t.integer  "setting_group_id"
    t.string   "key"
    t.string   "display_name"
    t.string   "hint"
    t.boolean  "has_user_changed", :default => false
    t.text     "value"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
  end

  create_table "events", :force => true do |t|
    t.string   "permalink"
    t.string   "party"
    t.string   "edition"
    t.datetime "date"
    t.string   "cover_image"
    t.text     "description"
    t.boolean  "has_guests"
    t.boolean  "list_closed"
    t.boolean  "show"
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
    t.boolean  "has_birthday_lists", :default => true
  end

  create_table "galleries", :force => true do |t|
    t.string   "party"
    t.string   "edition"
    t.string   "permalink"
    t.string   "cover_image"
    t.datetime "date"
    t.boolean  "show"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "guests", :force => true do |t|
    t.string   "email"
    t.string   "name"
    t.integer  "event_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "photos", :force => true do |t|
    t.string   "image"
    t.integer  "gallery_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
