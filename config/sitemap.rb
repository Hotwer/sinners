# -*- encoding : utf-8 -*-
# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "http://ohmnightclubla.com/"

SitemapGenerator::Sitemap.create do
  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #

  # add home_about_us_path, :priority => 0.9, :changefreq => 'monthly'
  # add home_management_path, :priority => 0.9, :changefreq => 'monthly'
  # add home_marketing_path, :priority => 0.9, :changefreq => 'monthly'
  # add home_events_path, :priority => 0.9, :changefreq => 'monthly'
  # add home_contact_path, :priority => 0.9, :changefreq => 'monthly'

  # Event.all.each do |e|
  #   add home_event_detail_path(e.permalink), :lastmod => e.created_at
  # end

  # Artist.all.each do |e|
  #   add home_artist_path(e.permalink), :lastmod => e.created_at
  # end

  # Post.all.each do |e|
  #   add home_post_path(e.permalink), :lastmod => e.created_at
  # end
end
