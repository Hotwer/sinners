lock '3.4'

set :application, 'sinners'
set :repo_url, 'git@bitbucket.org:Hotwer/sinners.git'
set :linked_dirs, %w{public/system public/uploads log}
set :linked_files, %w{config/database.yml config/application.yml}
set :local_dir, "/var/www/"
set :home_dir, '/home/deployer/'
set :tmp_dir, '/home/deployer/tmp'
set :nginx_conf_dir, '/home/deployer/nginx_conf'
set :user, "deployer"
set :port, 1026
set :use_sudo, false
set :ssh_options, {:forward_agent => true}

before "deploy", "deploy:create_database_file"
before "deploy", "deploy:create_application_file"
before "deploy", "digital_ocean:create_nginx_file"
after "deploy", "db:migrate"
after "deploy", "deploy:disallow_robots"
after "deploy", "deploy:add_env_file"
after "deploy", "deploy:restart_mod_rails"
after "deploy", "deploy:restart_mod_rails"
