# -*- encoding : utf-8 -*-
Sinners::Application.routes.draw do  
  root :to => 'home#index'
  mount BsAdmin::Engine => '/admin'
  
  match '/agenda(/:permalink)' => "home#agenda", as: :home_agenda
  match '/sobre' => "home#sobre", as: :home_sobre
  match '/fotos' => "home#fotos", as: :home_fotos
  match '/fotos/:permalink' => "home#fotos_detail", as: :home_fotos_detail  
  match '/contato' => "home#contato", as: :home_contato
  match '/aniversarios' => "home#aniversarios", as: :home_aniversarios
  get '/aniversarios/lista(/:permalink)' => "home#aniversarios_lista", as: :home_aniversarios_lista
  post  '/agenda/:permalink/lista' => "home#agenda_lista_post", as: :home_agenda_lista
  post  '/aniversarios/lista' => "home#aniversarios_lista_post", as: :home_aniversarios_lista_post
end
