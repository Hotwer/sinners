set :rails_env, "test"
set :branch, 'do-migration'
set :deploy_to, "/home/deployer/apps/test_#{fetch(:application)}"
set :local_shared_files_dir, "#{fetch(:local_dir)}/config/deploy/test"
set :capistrano_stage, fetch(:rails_env)

server '104.236.77.118', user: 'deployer', roles: %w{web app}, port: 1026

