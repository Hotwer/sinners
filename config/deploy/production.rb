set :rails_env, "production"
set :branch, 'master'
set :deploy_to, "/home/deployer/apps/production_#{fetch(:application)}"
set :local_shared_files_dir, "#{fetch(:local_dir)}/config/deploy/production"
set :capistrano_stage, fetch(:rails_env)

server '104.236.21.208', user: 'deployer', roles: %w{web app}, port: 1026
