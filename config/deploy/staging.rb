set :rails_env, "test"
set :branch, 'do-migration'
set :deploy_to, "/home/deployer/apps/staging_#{fetch(:application)}"
set :local_shared_files_dir, "#{fetch(:local_dir)}/config/deploy/staging"
set :capistrano_stage, "staging"

server '104.131.17.132', user: 'deployer', roles: %w{web app}, port: 1026
