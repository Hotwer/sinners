BsAdmin.config do |c|
  c.create_admin_for %w(Event Gallery)
  c.auto_populate %w(Event Gallery)
end  
