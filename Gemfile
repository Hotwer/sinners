source 'http://rubygems.org'

# core =====================================================
gem 'rails', '3.2.21'
gem 'mysql2'
gem 'thin', :group => :development
gem 'unicorn'
gem 'rake'
gem 'mailgun_rails', '0.8.0'
gem 'mail_view', '~> 2.0.4'
gem 'bower-rails', '~> 0.9.1'
gem 'stripe', '~> 1.20.1'
gem 'stripe_event'
gem 'sorcery'
# gem 'debugger'
gem 'launchy'

# debug and errors ==========================================
group :development do
  gem 'better_errors', '1.1.0'
end

group :development, :test do
  gem 'jazz_hands'
  gem 'pry-alias'
  gem 'binding_of_caller'
  # gem 'sprockets_better_errors'
end

# test ======================================================
group :development do
  gem 'rspec-rails'
  gem 'factory_girl_rails'
  gem 'capybara'
  gem 'poltergeist'
  gem 'capybara-webkit'
  gem 'formulaic'
  gem 'guard-rspec'
  gem 'json-schema'
  gem 'stripe-ruby-mock', '~> 2.1.1', :require => 'stripe_mock'
  gem 'jasmine'
  gem 'timecop'
end

# populate =================================================
gem 'faker'
gem 'populator'

# assets processors ========================================
gem 'uglifier', '>= 1.0.3'
gem 'therubyracer', :platform => :ruby, group: :development
gem 'sass', '3.2.10'
gem 'sass-rails'

gem 'compass'
gem 'compass-rails'

gem 'coffee-rails', '~> 3.2.1'
gem 'slim'
gem 'slim-rails'

# gem 'sprockets-rails', '=2.0.0.backport1'
# gem 'sprockets', '=2.2.2.backport2'
# gem 'sass-rails', github: 'guilleiguaran/sass-rails', branch: 'backport'

# assets libraries =========================================
gem 'jquery-rails'
gem 'jquery-ui-rails'
# gem 'jquery-easing-rails'
gem 'underscore-rails'
# gem 'jquery_mobile_rails'
gem 'jquery-fileupload-rails'
gem 'fancybox2-rails', '~> 0.2.4'
gem 'bootstrap-sass', '~> 3.1.1'
gem 'autoprefixer-rails', '5.1.7'
gem 'bootstrap-wysihtml5-rails'
gem 'summernote-rails'
# gem 'font-awesome-sass'
gem 'font-awesome-rails'
gem 'select2-rails' #admin

# image processing =========================================
gem 'rmagick'
gem 'carrierwave'
# gem 'piet', :path => '~/dev-projects/_lib/piet'
# gem 'piet', git: 'https://github.com/leouz/piet.git', :branch => 'master'
# gem 'piet-binary'

# main =====================================================
gem 'sitemap_generator'
gem 'figaro'


# model helpers ============================================
gem 'kaminari'
gem 'email_validator'
# gem 'strip_attributes'
gem 'acts-as-taggable-on'
# gem 'will_paginate', '~> 3.0'
# gem 'simple_form'
# gem 'twitter'
# gem 'twitter-text'
# gem 'instagram'
# gem 'omniauth-facebook'
# gem 'omniauth-google-oauth2'
gem 'money-rails'

# home made ================================================
# gem 'bs_admin', :path => '~/dev-projects/_lib/bs_admin'
# gem 'cms_base', :path => '~/dev-projects/_lib/cms_base'

gem 'bs_admin', git: 'https://github.com/leouz/bs_admin.git', :branch => 'sinners'
gem 'cms_base', git: 'https://github.com/leouz/cms_base.git', :branch => 'sinners'

# deploy ===================================================
gem 'capistrano', '~> 3.2.0'
gem 'capistrano-rails', '~> 1.1'
gem 'capistrano-rvm'
