class ContactMailer < ActionMailer::Base
  default from: "contato@sinnersclub.com.br"
  default reply_to: "contato@sinnersclub.com.br"

  def agenda_lista event, guests
    @event, @guests = event, guests  
    mail(:to => @guests[0].email,
         :subject => "Sinners Club | Confirmacao de envio de nomes para lista")
  end

  def aniversarios_lista event, birthday_list
    @event, @birthday_list = event, birthday_list
    mail(:to => @birthday_list.email,
         :subject => "Sinners Club | Confirmacao de envio de nomes para lista de aniversario")
 end
end
