module ApplicationHelper
  def bg_image_from_settings    
    bg_image = setting :index_page, :background    
    if params[:action] == "index" and !bg_image.nil?
      "background: url('#{bg_image}') no-repeat center center fixed;
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;"
    else
      ""
    end
  end

  def convert_to_video_embed_link link
    link = link.gsub('https://', '').gsub('http://', '')

    youtube_link = '//www.youtube.com/embed/'
    vimeo_link = '//player.vimeo.com/video/'
    
    result = vimeo_link + link.gsub('vimeo.com/', '') if link.include?('vimeo.com/')    
    result = youtube_link + link.gsub('www.youtube.com/watch?v=', '') if link.include?('www.youtube.com/watch?v=')    
    result = youtube_link + link.gsub('youtu.be/', '') if link.include?('youtu.be/')
    
    result   
  end
end
