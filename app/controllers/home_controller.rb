# -*- encoding : utf-8 -*-
require 'date'

class HomeController < ApplicationController
  def index
    load_events
  end

  def agenda
    load_events
    if params[:permalink]
      @active_event = event_by_permalink
    else
      @active_event = @events.first
    end
  end

  def sobre
    @photos = []
    (1..8).each do |i|
      image = BsAdmin::Settings.image(:sobre_page, "photo#{i}")
      @photos << image unless image.blank?
    end
  end

  def fotos
    @galleries = Gallery.order('date DESC').page(params[:page]).per(8)
  end

  def fotos_detail
    @gallery = Gallery.find_by_permalink(params[:permalink])
  end

  def aniversarios
  end

  def contato
  end

  def agenda_lista_post
    @event, @guests = event_by_permalink, []

    if params[:email].blank? || !is_a_valid_email(params[:email])
      return render :json => { error: 'invalid email' }
    end

    if @event.guests_list_open?
      params[:name].each do |name|
        unless name.blank?
          @guests << @event.guests.create({ email: params[:email], name: name })
        end
      end
    end
    # begin    
    ContactMailer.agenda_lista(@event, @guests).deliver
    # rescue
    # end

    render :json => ""
  end

  def is_a_valid_email(email)
    (email =~ /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i)
  end

  def aniversarios_lista
    load_events
    unless params[:permalink].blank?
      @event = event_by_permalink
      @birthday_list = @event.birthday_lists.new
    else
      @birthday_list = BirthdayList.new
    end
    @events_for_select = all_future_events.order('date').select{ |e| e.birthday_lists_open? }.map{ |e| [e.label_for_select, e.id] }
  end

  def aniversarios_lista_post
    unless params[:birthday_list][:date].blank?
      params[:birthday_list][:date] = Time.strptime(params[:birthday_list][:date], "%d/%m/%Y").in_time_zone('Brasilia')
    end

    birthday_list = BirthdayList.new(params[:birthday_list])
    date = birthday_list.date
    event_date = birthday_list.event.date

    if birthday_list.valid?
      if date >= event_date - 4.days and date <= event_date + 4.days
        if birthday_list.save
          # begin
            ContactMailer.aniversarios_lista(birthday_list.event, birthday_list).deliver
          # rescue
          # end
        end
        render :json => ""

      else
        render json: { errors: { message: "A data do aniversário deve ser 4 dias antes ou depois da festa." }}
      end
    else
      render json: { errors: { message: "Os campos Festa, Nome do Aniversariante e Data do Aniversário devem ser preenchidos." }}
    end
  end

  private

  def number_of_next_events
    BsAdmin::Settings.setting(:index_page, :next_events).to_i || 4
  end

  def event_by_permalink
    Event.find_by_permalink(params[:permalink])
  end

  def load_events
    @events = all_future_events.order('date').take number_of_next_events
    @events = @events.select{ |s| s.show }
  end

  def all_future_events
    yesterday = Time.zone.today - 12.hour
    Event.where("date >= ?", yesterday)
  end
end
