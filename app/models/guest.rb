class Guest < ActiveRecord::Base
  bs_admin do |c|
    c.belongs_to :event

    c.fields do |f|   
      f.string :name, required: true
      f.email :email, required: true      
    end

    c.index_fields [:name, :email]

    c.populate_batch_count 300..800
  end
end
