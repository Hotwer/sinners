class Gallery < ActiveRecord::Base    
  bs_admin do |c|     
    c.has_many :photos, view_type: :images

    c.fields do |f|   
      f.permalink :permalink
      f.string :party
      f.string :edition
      f.datetime :date
      f.image :cover_image
      f.checkbox :show
    end

    c.title_field :title

    c.index_fields [:party, :edition, :permalink, :cover_image, :date, :show]
    
    c.filter :monthly, :date

    c.populate_batch_count 40
  end

  def title
    party + " // " + edition
  end

  def formatted_date    
    "#{PT_BR_WEEKDAYS_SHORT[date.wday]} #{date.day} #{PT_BR_MONTHS_SHORT[date.month - 1]} #{date.year.to_s[2..4]}" 
  end

  def title_date
    party + " // " + formatted_date + " // " + edition
  end

end
