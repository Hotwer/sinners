class Photo < ActiveRecord::Base  
  bs_admin do |c|     
    c.belongs_to :gallery

    c.fields do |f|   
      f.image :image, required: true      
    end

    c.index_fields [:name, :date, :email]

    c.populate_batch_count 30..50
  end
end

