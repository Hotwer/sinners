class BirthdayList < ActiveRecord::Base
  attr_accessible :event_id
  validates_associated :event

  bs_admin do |c|     
    c.belongs_to :event

    c.fields do |f|   
      f.string :name, required: true
      f.date :date, required: true
      f.email :email, required: true
      f.string :guest1
      f.string :guest2
      f.string :guest3
      f.string :guest4
      f.string :guest5
      f.string :guest6
      f.string :guest7
      f.string :guest8
      f.string :guest9
      f.string :guest10
      f.string :guest11
      f.string :guest12
      f.string :guest13
      f.string :guest14
      f.string :guest15
    end

    c.index_fields [:name, :date, :email]
    
    c.filter :monthly, :when

    c.populate_batch_count 1..5
  end
end

