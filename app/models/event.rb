class Event < ActiveRecord::Base  
  bs_admin do |c|     
    c.has_many :guests
    c.has_many :birthday_lists

    c.fields do |f|   
      f.permalink :permalink
      f.string :party
      f.string :edition
      f.datetime :date
      f.image :cover_image
      f.wysi :description
      f.checkbox :has_guests
      f.checkbox :has_birthday_lists
      f.checkbox :list_closed
      f.checkbox :show
    end
    c.title_field :party
    c.index_fields [:party, :edition, :permalink, :cover_image, :date, :show]

    c.filter :monthly, :date

    c.custom_page :guests_compilation, { target: "_blank", layout: false, title: "Lista para Impressao" }

    c.populate_batch_count 40
  end
  
  def title
    party + " // " + edition
  end

  def formatted_date
    "#{PT_BR_WEEKDAYS_SHORT[date.wday]} #{date.day} #{PT_BR_MONTHS_SHORT[date.month]} #{date.year.to_s[2..4]}" 
  end

  def title_date
    title + " // " + formatted_date
  end

  def label_for_select
    party + " // #{PT_BR_WEEKDAYS_SHORT[date.wday]} #{date.day}"
  end

  def twitter_link     
    r  = "http://platform.twitter.com/widgets/tweet_button.html?"
    r += "url=#{URI::join(BsAdmin::Settings.setting(:social, :base_url), permalink)}"
    r += "&text=#{party} // #{edition} @sinnersclub. #{date.strftime("%A, %B")} #{date.day.ordinalize}."
    r
  end

  def facebook_link    
    r  = "http://www.facebook.com/plugins/like.php"
    r += "?href=#{URI::join(BsAdmin::Settings.setting(:social, :base_url), permalink)}"
    r += "&width"
    r += "&layout=button_count"
    r += "&action=like"
    r += "&show_faces=false"
    r += "&share=false"
    r += "&height=21"
    r += "&appId=#{BsAdmin::Settings.setting(:social, :facebook_app_id)}"
    r
  end

  def google_plus_link
    r  = "https://plusone.google.com/_/+1/fastbutton?bsv"
    r += "&size=medium"
    r += "&hl=en-US"
    r += "&url=#{URI::join(BsAdmin::Settings.setting(:social, :base_url), permalink)}"
    r += "&parent=#{BsAdmin::Settings.setting(:social, :base_url)}"
    r    
  end

  def guests_list_open?
    has_guests and !list_closed
  end

  def birthday_lists_open?
    has_birthday_lists and !list_closed
  end
end
