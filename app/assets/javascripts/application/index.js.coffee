$(document).ready ->
  if $('body').hasClass('action-agenda')
    $('html, body').animate({ scrollTop: $("#col1").offset().top - 10 }, 1300)

    $('#lista-desconto-btn').click () ->
      $('.modal-lista-desconto.bg').fadeIn(500, () -> $('.modal-lista-desconto.modal').fadeIn(200))

    $('#cancelar-lista-desconto, .modal-lista-desconto.bg').click () ->
      $('.clear').val('')
      $('.modal-lista-desconto.modal').fadeOut(200, () -> $('.modal-lista-desconto.bg').fadeOut(500))

    $(document).on 'click', '#enviar-lista-desconto', ->
      $(".modal-lista-desconto form").ajaxSubmit
        type: 'post'
        success: (response) ->
          if response && response.error
            alert 'Por favor, digite um  e-mail válido!'
          else 
            alert "Nomes adicionados a lista! :)"
            $('#cancelar-lista-desconto').click()
        error: () ->
          alert "Ops! Aconteceu alguma coisa de errado!"

  if $('body').hasClass('action-aniversarios_lista')
    $(".datepicker").datepicker({ dateFormat: "dd/mm/yy" })

    $(document).on 'click', '#enviar-lista-aniversario', ->
      $("form").ajaxSubmit
        type: 'post'
        success: () ->
          alert "Lista de Aniversário enviada :)"
          $('input').val('')
        error: (data) ->
          alert data.errors.message

  if $('body').hasClass('action-index')
    $(window).resize () ->
      $('#flyer').height($(window).height() - 90 - 50 - 138 - 25)
    $(window).resize()

    $('.event').hover(
      () -> $('#flyer').css('background-image', "url('#{$(this).attr('cover-image')}')"),
      () -> $('#flyer').css('background-image', '')
    )

  $("#menu-mobile-right").click ->
    $("#menu-mobile-bg").fadeIn 700
    $("#menu-mobile").fadeIn 700
    $("#menu-mobile-right-close").fadeIn 700
    $("#menu-mobile-right").fadeOut 700

  $("#menu-mobile-right-close").click ->
    $("#menu-mobile-bg").fadeOut 700
    $("#menu-mobile").fadeOut 700
    $("#menu-mobile-right-close").fadeOut 700
    $("#menu-mobile-right").fadeIn 700

  if $('#is-touch').hasClass('is-touch')
    $(".fancybox").photoSwipe(
      getImageSource: (el) -> $(el).attr('fancybox-content')
      zIndex: 5000
    )
  else
    $(".fancybox").click () ->
      clicked = $(this).attr('fancybox-content')
      links = []
      i = 0
      initialPosition = 0

      $(".fancybox").each () ->
        link = $(this).attr('fancybox-content')
        links.push(link)
        initialPosition = i if link == clicked
        i += 1

      $.fancybox.open(links, { index: initialPosition })

  $(".fancybox-video").fancybox(
    type: 'iframe'
    fitToView : true
  )
